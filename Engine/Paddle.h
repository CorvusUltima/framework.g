#pragma once
#include"Ball.h"
#include"Vec2.h"
#include"RectF.h"
#include"Colors.h"
#include"Graphics.h"
#include"Keyboard.h"


class Paddle
{
public:
	Paddle(const Vec2& pos_in, float halfWidth_in, float halfHeight_in);
	void Draw(Graphics& gfx)const;
	bool DoWallCoalision(const RectF&  walls);
	bool DoBallColaision( Ball& ball)const;
	void Update(const Keyboard& kbd,float dt);
	RectF GetRect()const;


private:
	RectF rect;
	Color color = Colors::White;
	Color WingColor = Colors::Red;
	float wingWidth = 10.0f;
	float halfWidth;
	float halfHeight;
	Vec2 pos;
	float speed = 300.0f; 





};