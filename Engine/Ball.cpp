#include "Ball.h"
#include"SpriteCodex.h"
#include"Vec2.h"

Ball::Ball(const Vec2 pos_in, const Vec2 vel_in)
	:
pos(pos_in),
vel(vel_in)

{
}

void Ball::Draw(Graphics& gfx) const
{
	SpriteCodex::DrawBall(pos, gfx);
}

void Ball::Update(float dt)
{
	pos += vel * dt;
}

bool Ball::WallCoalision(const RectF& walls)
{
	bool colided = false;
	const RectF rect = GetRect();

	if (rect.left < walls.left)
	{

		pos.x += walls.left - rect.left;
		ReboudnX();
		colided = true;
	}
	else if (rect.right > walls.right)
	{
		pos.x -= rect.right - walls.right;
		ReboudnX();
		colided = true;
	}

	if (rect.top < walls.top)
	{
		pos.y += walls.top- rect.top;
		ReboundY();
		colided = true;

	}
	else if (rect.bottom > walls.bottom)
	{
		pos.y -= rect.bottom - walls.bottom;
		ReboundY();
		colided = true;

	}
	return colided;
}

void Ball::ReboudnX()
{
	vel.x = -vel.x;
}

void Ball::ReboundY()
{
	vel.y = -vel.y;
}

RectF Ball::GetRect() const
{
	return RectF::FromCentar(pos, radius, radius);
}

Vec2 Ball::GetVelocity()
{
	return vel;
}

