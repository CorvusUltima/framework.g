#pragma once
#include"RectF.h"
#include"Vec2.h"
#include"Graphics.h"

class Ball

{
public:

	Ball(const Vec2 pos_in, const Vec2 vel_in);
	void Draw(Graphics& gfx)const;
	void Update( float dt);
	bool WallCoalision(const RectF& walls);
	void ReboudnX();
	void ReboundY();
	RectF GetRect()const;
	Vec2 GetVelocity();
	
   


private:
	static constexpr float  radius =7;
	Vec2 pos;
	Vec2 vel;






};